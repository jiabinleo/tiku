const HtmlWebpackPlugin = require("html-webpack-plugin");
const Mock = require('mockjs')
const fs = require("fs")
const glob = require("glob")
function getEntry() {
    var entry = [];
    let exporthtml = "cat4grade1"
    glob.sync('./src/pages/*/*/index.ts')
        .forEach(function (file) {
            let template = file.split("pages/")[1].split("/index.ts")[0].replace("/", "_");
            let template_ = file.split("pages/")[1].split("/index.ts")[0].split("/")[0];
            if (exporthtml) {
                if (template_ == exporthtml) {
                    entry.push(template)
                }
            } else {
                entry.push(template)
            }

        });
    return entry;
};
console.log(getEntry())
const pages = getEntry();
let htmlTemplate = [];
let entry = {};
let data = {}
const defaultTemplate = [];
const defaultEntry = {};
const defaultData = {}
for (let index = 0; index < pages.length; index++) {
    let page = pages[index];
    entry[page] = `./src/pages/${page.split("_")[0]}/${page.split("_")[1]}/index.ts`
    data[page] = `./src/pages/${page.split("_")[0]}/${page.split("_")[1]}/mock.js`
    htmlTemplate.push(new HtmlWebpackPlugin({
        template: `./src/pages/${page.split("_")[0]}/${page.split("_")[1]}/index.ejs`,
        hash: false,
        minify: {
            caseSensitive: true, //以大小写敏感的方式处理属性 (对自定义html标签很有用)
            collapseWhitespace: true, //去除空格对文本节点有益
            keepClosingSlash: true,	//保留单个元素的尾部斜杠
            removeComments: true, //删除html注释
            removeRedundantAttributes: false, //删除属性，如果它的值和默认值一致.
            removeScriptTypeAttributes: true, //删除script标签上的type="text/javascript" . 否则 type 属性值会保留
            removeStyleLinkTypeAttributes: true, //删除style标签的type="text/css"  否则 type 属性值会保留
            useShortDoctype: true, //使用html5 简短的doctype 
            continueOnParseError: true, //处理解析错误 而不是终止
            minifyJS: true,
            minifyCSS: true,
            preserveLineBreaks: false,//保留标签之间有1个空行. 必须和 collapseWhitespace=true一起使用
            sortAttributes: true,
            sortClassName: true,
            trimCustomFragments: false//删除ignoreCustomFragments两端空白.
        },
        inject: "body",
        filename: `${page}.html`,
        xhtml: true,
        showErrors: true,
        chunks: [page]
    }))
}
for (const key in data) {
    if (Object.hasOwnProperty.call(data, key)) {
        fs.access(data[key], (err) => {
            if (!err) {
                let file = require(data[key])
                for (let i = 0; i < pages.length; i++) {
                    if (pages[i].template == key) {
                        pages[i].mock = true
                    }
                }
                fs.writeFile(data[key].split(".js")[0] + ".json", JSON.stringify(Mock.mock(file)), function (err) {
                    if (err) {
                        console.error(`${key}创建失败`);
                        return console.log(err)
                    }
                })
            }
        })
    }
}
setTimeout(() => {
    console.table(pages)
}, 3000)
let defaultpages = ['login', 'state'];
for (let s = 0; s < defaultpages.length; s++) {
    entry[defaultpages[s]] = `./src/pages/${defaultpages[s]}/index.ts`;
    htmlTemplate.push(new HtmlWebpackPlugin({
        template: `./src/pages/${defaultpages[s]}/index.ejs`,
        hash: false,
        minify: {
            caseSensitive: true, //以大小写敏感的方式处理属性 (对自定义html标签很有用)
            collapseWhitespace: true, //去除空格对文本节点有益
            keepClosingSlash: true,	//保留单个元素的尾部斜杠
            removeComments: true, //删除html注释
            removeRedundantAttributes: false, //删除属性，如果它的值和默认值一致.
            removeScriptTypeAttributes: true, //删除script标签上的type="text/javascript" . 否则 type 属性值会保留
            removeStyleLinkTypeAttributes: true, //删除style标签的type="text/css"  否则 type 属性值会保留
            useShortDoctype: true, //使用html5 简短的doctype 
            continueOnParseError: true, //处理解析错误 而不是终止
            minifyJS: true,
            minifyCSS: true,
            preserveLineBreaks: false,//保留标签之间有1个空行. 必须和 collapseWhitespace=true一起使用
            sortAttributes: true,
            sortClassName: true,
            trimCustomFragments: false//删除ignoreCustomFragments两端空白.
        },
        inject: "body",
        filename: `${defaultpages[s]}.html`,
        xhtml: true,
        showErrors: true,
        chunks: [defaultpages[s]]
    }))
}

module.exports = {
    entry,
    htmlTemplate
}