import "./index.less";
declare let $: any;
declare let layui: any;
declare let layer: any;

export default $(function(){
    layui.use('upload', function(){
        layui.upload.render({
            elem: '.upload-wrap', //绑定元素
            url: "https://zhaosheng.ieduchina.com/zsmanager/Api/uploadFile", //上传接口
            field:"photo",
            done: function(res,index){ //上传成功后的回调
                if(res.status == 1){
                    $(this.item).css("background-image","url('http://zhaosheng.ieduchina.com"+res.data+"')");
                    $(this.item).closest(".upload").find("input[type='hidden']").val(`http://zhaosheng.ieduchina.com${res.data}`);
                }else{
                    layer('上传失败');
                }
            },
            error(err){
                console.log(err)
            }
        });
    })
});
