import "./index.less";
declare let $: any;
export default $(function () {
    let rotateIndex = 0;
    $("#preview").on("click", function () {
        $(this).fadeOut(200,function(){
            $(this).find("img").removeAttr("class");
            rotateIndex = 0
        });
    });
    $("img[preview]").on("click", function () {
        $("#preview").find("img").attr("src", $(this).attr("src"));
        $("#preview").addClass("show");
        $("#preview").fadeIn(200);
    });
    var inputs = $(".right ol").find("input");
    $.each(inputs, function (index, element) {
        element.oninput = function (el) {
            let num = 0;
            // if (!($(el.target).val() == "")) {
            //     num = +$(el.target).val();
            // }
            // if (num > 0) {
            //     $(el.target).val(num);
            // } else {
            //     $(el.target).val(0);
            // }
            getSum();
        };
    });
    getSum();
    function getSum() {
        var sum = 0;
        $.each(inputs, function (index, el) {
            if (index == 0) {
                sum = 0;
            }
            if (el.value >= 0) {
                sum += +el.value;
            }
            if (index == inputs.length - 1) {
                $(".right").find("p input").val(sum);
            }
        });
    }
    function toast(title, time) {
        var $textToast = $('#textToast');
        if ($textToast.css('display') != 'none') return;
        if (title) {
            $(".weui-toast__content").text(title);
        }
        if (!Number(time)) {
            time = 2000;
        }
        $textToast.fadeIn(100);
        setTimeout(function () {
            $textToast.fadeOut(100);
        }, time);
    }
    $(document).on("click", "button[type='submit']", function (e) {
        e = e || window.event;
        e.preventDefault();
        var form = $(this).parents('form');
        var action = form.attr("action");
        var type = form.attr("method");
        var content = form.serialize();

        $.ajax({
            url: action + "&t=" + Math.random(),
            type: type,
            dataType: 'json',
            data: content,
            header: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
                'X-XXS-Protection': '1;mode=block',
                'X-Frame-Options': 'deny'
            },
            success: function success(data) {
                toast("提交成功",2000);
                setTimeout(function () {
                    window.location.href = "/zsmanager/inter_student/cepin/";
                }, 2000)

            },
            error: function error() {
                toast("提交失败",2000);
            }
        });
    });
    $("#preview").on("click","span",function(){
        if($(this).hasClass("left")){
            rotateIndex-=1
        }else if($(this).hasClass("right")){
            rotateIndex+=1
        }
        if(rotateIndex<0){
            rotateIndex = 3
        }
        rotateIndex = Math.abs(rotateIndex)%4;
        $(this).siblings("img").removeAttr("class").addClass("ratate"+rotateIndex)
        return false
    })
});