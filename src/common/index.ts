import "@cs/index_msg/index.ts";
import "@cs/index_upload/index.ts";
import "@cs/index_header/index.ts";
import "@cs/index_footer/index.ts";
declare let $: any;
export default {}
$(function () {
  function toast(title: string) {
    var $textToast = $('#textToast');
    if ($textToast.css('display') != 'none') return;
    if (title) {
      $(".weui-toast__content").text(title);
    }
    $textToast.fadeIn(100);
    setTimeout(function () {
      $textToast.fadeOut(100);
    }, 2000);
  }
  let apply = true;
  $(document).on("click", "button[type='submit']", function (e: any) {
    e = e || window.event
    e.preventDefault();
    var form = $(this).parents('form');
    let action = form.attr("action");
    let type = form.attr("method");
    let result = false;
    form.find('input, textarea').each(function () {
      result = check(this);
      return result;
    });
    let content = form.serialize()
    if (result && apply) {
      apply = false;
      $.ajax({
        url: action + "&t=" + Math.random(),
        type: type || 'POST',
        dataType: 'json',
        data: content,
        header: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
          'X-XXS-Protection': '1;mode=block',
          'X-Frame-Options': 'deny'
        },
        success: function (data: any) {
          if (data.status == 1) {
            toast(data.info)
            setTimeout(function () {
              window.location.href = data.url
            }, 1500)
          } else {
            toast(data.info)
            apply = true
            return false
          }

        },
        error: function error() {
          toast("提交失败,请稍后重试")
          apply = true;
          return false
        }
      })
    }
  })
  $("#year").text(new Date().getFullYear())
  $(document).keyup(function (event) {
    if (event.keyCode == 13) {
      $("button[type='submit']").trigger("click");
    }
  });
  // 获取验证码
  var countdown = 60;
  let timer = 0;
  $("button.getcode").on("click", function (e) {
    e = e || window.event;
    e.preventDefault();
    var $this = $(this);
    var text = $(this).text();
    var mobile = $(this).closest("form").find("input[name='mobile']").val();
    if (countdown == 60) {
      if (!$.trim(mobile).length) {
        toast("请输入手机号")
        return false;
      }
      if (!/^[1][0-9]{10}$/.test(mobile)) {
        toast('手机号格式不正确')
        return false;
      }
      var content = {
        mobile: $.trim(mobile)
      };
      $.ajax({
        url: "/index/cepin/login.html?do=sendmobilecode&t=" + Math.random(),
        type: 'POST',
        dataType: 'json',
        data: content,
        success: function (data) {
          if (data != 1) {
            toast("验证码发送失败")
          } else {
            toast("验证码发送成功，请查看手机")
          }
          $this.text(countdown + ' s').attr('disabled', "true");
          countdown--;
          timer = window.setInterval(function () {
            if (countdown > 0) {
              $this.text(countdown + ' s');
              countdown--;
            } else {
              $this.text(text).removeAttr('disabled');
              clearInterval(timer);
              countdown = 60;
            }
          }, 1000);
        },
        error: function () {
          toast("验证码发送失败");
        }
      });
      return false;
    }
  });
  var dataReg = {
    m: /^[1][0-9]{10}$/
  };
  function check(element) {
    var datatype = $(element).attr('datatype');
    var value = $(element).val();
    var data = $(element).attr("data");
    var siblings = null;

    if (($(element).attr("type") == "radio" || $(element).attr("type") == "checkbox") && $(element).attr("name")) {
      value = $("input[name='" + $(element).attr("name") + "']:checked").val();
      if (!value) {
        if ($(element).attr("nullmsg")) {
          toast($(element).attr("nullmsg"))
        }
        $('html, body').animate({
          scrollTop: Math.ceil($(element).closest("li").offset().top)
        }, 300);
        return false;
      }
    } else if (($(element).attr("type") == "text" || $(element).attr("type") == "textarea") && $(element).attr("name")) {
      value = $(element).val();
      siblings = $(element).closest("li").find(".upload").find("input[type='hidden']").val();
      if (!value && !siblings) {
        if ($(element).attr("nullmsg")) {
          toast($(element).attr("nullmsg"))
        }
        if ($(element).closest("li").length) {
          $('html, body').animate({
            scrollTop: Math.ceil($(element).closest("li").offset().top)
          }, 300);
        }
        return false;
      }
    } else if ($(element).attr("type") == "hidden") {
      value = $(element).val();
      siblings = $(element).closest("li").find("textarea[name]").val();
      if (!siblings) {
        siblings = $(element).closest("li").find("input[name]").val();
      }
      if (!value && !siblings) {
        if ($(element).attr("nullmsg")) {
          toast($(element).attr("nullmsg"))
        }
        $('html, body').animate({
          scrollTop: Math.ceil($(element).closest("li").offset().top)
        }, 300);
        return false;
      }
    }
    return true;
  }
})